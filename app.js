const express = require('express')
const app = express()
const port = 5000
const Sentry = require('@sentry/node');

Sentry.init({
  dsn: process.env.SENTRY_DSN,
});

app.get('/', (req, res) => {
  Sentry.captureException(new Error('Good bye'));
  res.send('Hello World! Sentry DSN=' + process.env.SENTRY_DSN)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
